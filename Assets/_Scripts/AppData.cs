﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppData : Singleton<AppData>
{
    public static AppData Instance
    {
        get
        {
            return ((AppData)mInstance);
        }
        set
        {
            mInstance = value;
        }
    }

    public void SaveCurrentTour(TourView currentTour)
    {
        //Mapping TourView to TourModel
        TourModel tourModel = new TourModel();
        tourModel.Name = currentTour.Name;
        for (int i = 0; i < currentTour.TourScenes.Count; i++)
        {
            TourSceneView tourSceneview = currentTour.TourScenes[i];
            tourModel.TourScenes.Add(new TourSceneModel());
            tourModel.TourScenes[i].Name = tourSceneview.Name;
            tourModel.TourScenes[i].PanoData = tourSceneview.panorama.EncodeToPNG();
            tourModel.TourScenes[i].AudioData = BinaryController.GetBytesFromAudioClip(tourSceneview.audio);
            for (int j = 0; j < tourSceneview.InteractivePoints.Count; j++)
            {
                InteractivePointView interactivePointView = tourSceneview.InteractivePoints[j];
                tourModel.TourScenes[i].InteractivePoints.Add(new InteractivePointModel()
                {
                    Position = new PointPosition()
                    {
                        x = interactivePointView.Position.x,
                        y = interactivePointView.Position.y,
                        z = interactivePointView.Position.z
                    },

                    Content = new ContentImageModel()
                    {
                        Data = interactivePointView.image.texture.EncodeToPNG()
                    }
                });

            }
        }
        BinaryController.SerializeTour(tourModel);
        Debug.Log("Saved!");
    }
}

#region TourModel for Serialization
[Serializable]
public class TourModel
{
    public TourModel()
    {
        TourScenes = new List<TourSceneModel>();
    }
    public string Name { get; set; }
    public List<TourSceneModel> TourScenes { get; set; }
}

[Serializable]
public class TourSceneModel
{
    public TourSceneModel()
    {
        InteractivePoints = new List<InteractivePointModel>();
    }
    public string Name { get; set; }
    public byte[] PanoData { get; set; }
    public byte[] AudioData { get; set; }
    public List<InteractivePointModel> InteractivePoints { get; set; }
}

//We have to change Vector3 to distance from camera
[Serializable]
public class InteractivePointModel
{
    public ContentImageModel Content { get; set; }
    public PointPosition Position { get; set; }
}

[Serializable]
public struct ContentImageModel
{
    public byte[] Data { get; set; }
}
#endregion

#region TourView
public class TourView
{
    public TourView()
    {
        TourScenes = new List<TourSceneView>();
    }
    public string Name { get; set; }
    public List<TourSceneView> TourScenes { get; set; }
}
public class TourSceneView
{
    public TourSceneView()
    {
        InteractivePoints = new List<InteractivePointView>();
    }
    public string Name { get; set; }
    public Texture2D panorama { get; set; }
    public AudioClip audio { get; set; }
    public List<InteractivePointView> InteractivePoints { get; set; }
}
//We have to change Vector3 to distance from camera
public class InteractivePointView
{
    public Sprite image;
    public Vector3 Position { get; set; }
}
#endregion

[Serializable]
public struct PointPosition
{
    public float x { get; set; }
    public float y { get; set; }
    public float z { get; set; }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PanelsController : MonoBehaviour {
    public UIAnimator[] Panels;
	void Update () {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            CloseAllPanels();
        }
	}
    public void CloseOtherPanels(UIAnimator exeptionPanel)
    {
        foreach (UIAnimator animator in Panels)if(animator!=exeptionPanel) animator.PopBack();    
    }
    public void CloseAllPanels()
    {
        foreach (UIAnimator animator in Panels) animator.PopBack();    
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SaveDialog : MonoBehaviour {

    public GameObject saveDialogObj;
    public InputField tourName;
    public TourController tourController;
    public void ShowPanel()
    {
        saveDialogObj.SetActive(true);
    }
    public void ClosePanel()
    {
        saveDialogObj.SetActive(false);
    }
    public void SaveTour()
    {
        if (tourName.text != "")
        {
            tourController.SaveTour(tourName.text);
            ClosePanel();
        }
    }
}

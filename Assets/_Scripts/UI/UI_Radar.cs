﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UI_Radar : MonoBehaviour {

    public Transform camera;
    public RectTransform radarSector;
    public Text currentCameraAngle;

    void Update()
    {
        if (camera.localEulerAngles.y < 180) currentCameraAngle.text = Convert.ToInt32(camera.localEulerAngles.y).ToString() + "°";
        else { currentCameraAngle.text = (-(360 - Convert.ToInt32(camera.localEulerAngles.y))).ToString() + "°"; }
        radarSector.localEulerAngles = new Vector3(0, 0, -camera.localEulerAngles.y);
    }
}

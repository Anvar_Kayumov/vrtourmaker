﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UI_Point : MonoBehaviour {

    public Image Image_Point;
    public Image Image_Content;
    private TourController _tourController;

    void Start()
    {
        _tourController = FindObjectOfType<TourController>() as TourController;
    }

    public void ShowContent()
    {
        Image_Point.raycastTarget = false;
        Image_Content.raycastTarget = true;
        Image_Point.transform.DOScale(0, 0.2f).OnComplete(() =>
        {
            Image_Content.transform.DOScale(3, 0.2f);
        });
    }
    
    public void HideContent()
    {
        Image_Point.raycastTarget = true;
        Image_Content.raycastTarget = false;
        Image_Content.transform.DOScale(0, 0.2f).OnComplete(() => {
            Image_Point.transform.DOScale(0.6f, 0.2f);
        });
    }

    public void EditPointPosition()
    {
        _tourController.EditPointPosition(Image_Content);
        Destroy(this.gameObject);
    }

    public void DeleteThisPoint()
    {
        _tourController.DeletePoint(Image_Content);
        Destroy(this.gameObject);
    }
    

}

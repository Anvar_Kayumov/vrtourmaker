﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum Axis
{
    None, x, y
}

public enum PopState
{
    POPINGUP, POPINGBACK
}

public class UIAnimator : MonoBehaviour {

    public RectTransform popUpGameObject;
    public float time;
    public float startPosition;
    public float endPosition;
    public Axis _axis;
    public PanelsController panelsController;
    public PopState popState = PopState.POPINGBACK;

    public void tooglePop()
    {
        if (popState == PopState.POPINGBACK)
            PopUp();
        else PopBack();
    }
    public void PopUp()
    {
        popState = PopState.POPINGUP;
        panelsController.CloseOtherPanels(this);
        if (_axis.ToString() == "y") popUpGameObject.DOMoveY(endPosition, time, false);
        else if (_axis.ToString() == "x") popUpGameObject.DOMoveX(endPosition, time, false);
    }
    public void PopBack()
    {
        popState = PopState.POPINGBACK;
        if (_axis.ToString() == "y") popUpGameObject.DOMoveY(startPosition, time, false);
        else if (_axis.ToString() == "x") popUpGameObject.DOMoveX(startPosition, time, false);
    }

}

﻿using UnityEngine;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class UI_StreamingPanel : MonoBehaviour {

  //  public GameObject HTCCamera;
  //  public GameObject OculusCamera;
    public GameObject StreamingPanel;
    public Image rightTopBtn;

   
    public void Reset()
    {
        StreamingDisposed();
       // OculusCamera.SetActive(false);
       // HTCCamera.SetActive(false);
    }

    public void StartStreamingToHTCVive(bool toglleValueIsOn)
    {
        Reset();
        if (toglleValueIsOn)
        {
            StreaminhgActive();
         //   HTCCamera.SetActive(true);
        }
    }

    public void StartStreamingToOculusRift(bool toglleValueIsOn)
    {
        Reset();
        if (toglleValueIsOn)
        {
            StreaminhgActive();
          //  OculusCamera.SetActive(true);
        }
    }

    public void StartStreamingToCardboard(bool toglleValueIsOn)
    {
        Reset();
        if (toglleValueIsOn)
        {
            StreaminhgActive();
        }
    }

    public void StreaminhgActive()
    {
        rightTopBtn.color = Color.green;
    }

    public void StreamingDisposed()
    {
        rightTopBtn.color = Color.white;
    }

    public void ShowPanel()
    { 
        StreamingPanel.SetActive(!StreamingPanel.active);
    }
}

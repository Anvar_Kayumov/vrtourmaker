﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourController : MonoBehaviour {

    public GameObject listItem_Scenes;
    public Material mainPanoMaterial;
    public UI_FileBrowser ui_FileBrowser;
    public PanelsController panelsController;
    public Transform pointPlace;
    public GameObject pointObj;

    private TourView _tourView;
    private TourSceneView _currentSceneView;
    private GameObject _selectedSceneButton;
    private InteractivePointView _currentInteractivePointView;
    private List<GameObject> _inScenePointObjsList;

    //Need to change (connect with interface)
    void Start()
    {
        _tourView = new TourView();
        _tourView.Name = "TestTour";
        _inScenePointObjsList = new List<GameObject>();
    }

    //Need to change (Reduce code)
    public void OpenFileBrowser(string fileOpenFilter)
    {
        if (_tourView.TourScenes.Count != 0)
        {
            if (fileOpenFilter == "image")
            {
                ui_FileBrowser.fileOpenFilter = FILEOPENFILTER.IMAGE;
                ui_FileBrowser.ChangeFilters(new string[] { "jpg", "png", "jpeg" });
                panelsController.Panels[0].PopUp();
            }
            else if (fileOpenFilter == "audio")
            {
                ui_FileBrowser.fileOpenFilter = FILEOPENFILTER.AUDIO;
                ui_FileBrowser.ChangeFilters(new string[] { "mp3", "ogg", "wav" });
                panelsController.Panels[0].PopUp();
            }
            else if (fileOpenFilter == "panorama")
            {
                ui_FileBrowser.fileOpenFilter = FILEOPENFILTER.PANORAMA;
                ui_FileBrowser.ChangeFilters(new string[] { "jpg", "png", "jpeg" });
                panelsController.Panels[0].PopUp();
            }
        }
    }
    
    #region Scene
    public void SetImageToSceneListItem(Texture2D tex)
    {
        _selectedSceneButton.transform.GetChild(0).GetComponent<RawImage>().texture = tex;
    }

    public void SwitchToScene(int index)
    {
        _currentSceneView = _tourView.TourScenes[index];
        mainPanoMaterial.SetTexture("_MainTex",_currentSceneView.panorama);
    }

    public void AddNewSceneToList()
    {
        _tourView.TourScenes.Add(new TourSceneView());
        GameObject item = Instantiate(listItem_Scenes, Vector3.zero, Quaternion.identity) as GameObject;
        item.transform.SetParent(listItem_Scenes.transform.parent);
        int index = _tourView.TourScenes.Count-1;
        item.transform.GetChild(1).GetComponent<InputField>().text = "Scene " + (index+1);
        item.transform.GetChild(1).GetComponent<InputField>().onEndEdit.AddListener((newName) => RenameScene(newName));
        item.GetComponent<Button>().onClick.AddListener(() =>
        {
            _currentSceneView = _tourView.TourScenes[index];
            _currentSceneView.Name = "Scene " + (index + 1);
            SwitchToScene(index);
            _selectedSceneButton = item;
            LocateExistingPoints();
        });
        item.transform.SetSiblingIndex(index); //Change the index of an object in the hierarchy
        item.SetActive(true);
    }

    public void ChangePaonorama(Texture2D tex)
    {
        if (_currentSceneView != null)
        {
            _tourView.TourScenes.Find(x => x == _currentSceneView).panorama = tex;
            _currentSceneView.panorama = tex;
            mainPanoMaterial.SetTexture("_MainTex", tex);
            SetImageToSceneListItem(tex);
        }
    }

    public void RenameScene(string newSceneName)
    {
        Debug.Log(newSceneName);
        _currentSceneView.Name = newSceneName;
    }

    #endregion

    #region Interactive Point
    public void CreateInteractivePoint()
    {
        pointPlace.gameObject.SetActive(true);
    }

    public void FixPoint()
    {
        pointPlace.gameObject.SetActive(false);
        GameObject item = Instantiate(pointObj, pointPlace.position, pointPlace.rotation) as GameObject;
        _tourView.TourScenes.Find(x => x == _currentSceneView).InteractivePoints.Find(x => x == _currentInteractivePointView).Position = pointPlace.position;
        _currentInteractivePointView.Position = pointPlace.position;
        item.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = _currentInteractivePointView.image;
        _inScenePointObjsList.Add(item);
    }

    public void AddImageToPoint(Sprite sprite)
    {
        if (sprite != null)
        {
            InteractivePointView interactPointView = new InteractivePointView() { 
            image=sprite
            };
            _tourView.TourScenes.Find(x => x == _currentSceneView).InteractivePoints.Add(interactPointView);
            _currentInteractivePointView = interactPointView;
            CreateInteractivePoint();
        }
    }

    public void LocateExistingPoints()
    {
       DestroyAllPointsInScene();
       TourSceneView currentScene=_tourView.TourScenes.Find(x=>x==_currentSceneView);
       for (int i = 0; i < currentScene.InteractivePoints.Count; i++)
       {
           Vector3 pointPosition = currentScene.InteractivePoints[i].Position;
           GameObject item = Instantiate(pointObj, pointPosition, Quaternion.identity) as GameObject;
           item.transform.LookAt(2*pointPosition-Camera.main.transform.position);
           item.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = currentScene.InteractivePoints[i].image;
           _inScenePointObjsList.Add(item);
       }
    }

    public void DestroyAllPointsInScene()
    {
        for (int i = 0; i < _inScenePointObjsList.Count; i++)
        {
            Destroy(_inScenePointObjsList[i]);
        }
        _inScenePointObjsList.Clear();
    }

    public void EditPointPosition(Image pointContent)
    {
        _currentInteractivePointView = _currentSceneView.InteractivePoints.Find(x => x.image == pointContent.sprite);
        CreateInteractivePoint();
    }

    //Have to rewrite!
    public void DeletePoint(Image pointContent)
    {
        InteractivePointView interactivePointView = _currentSceneView.InteractivePoints.Find(x=>x.image==pointContent.sprite);
        _tourView.TourScenes.Find(x=>x==_currentSceneView).InteractivePoints.Remove(interactivePointView);
        _currentSceneView.InteractivePoints.Remove(interactivePointView);
    }
    #endregion

    public void SaveTour(string tourName)
    {
        if (_tourView.TourScenes.Count != 0)
        {
            _tourView.Name = tourName;
            AppData.Instance.SaveCurrentTour(_tourView);
        }
    }
}

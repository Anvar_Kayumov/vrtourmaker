﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AppFilesController : MonoBehaviour {

    private string[] folders = new string[] { "Tours" };

    void Awake()
    {
        foreach (string folder in folders)
        {
            if (!Directory.Exists(Application.dataPath + "/../" + folder))
            {
                Directory.CreateDirectory(Application.dataPath + "/../" + folder);
            }
        }
    }
}

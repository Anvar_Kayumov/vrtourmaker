﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System.Threading;
public struct ThumbData
{
 
    public byte[] _bytes;
    public string _fullName;
    public string _name;
    public float SizeX;
    public float SizeY;
}
public enum FILEOPENFILTER
{
    AUDIO,
    PANORAMA,
    IMAGE
}
public class UI_FileBrowser : MonoBehaviour
{
    #region Variables
	private string StartingDirectory = "";
	public FileBrowser _FB;
    public Text pathText;
    public RectTransform MainPanel; 
    public Button MenuItem;
    public PanelsController panelsController;
    public TourController tourController;
    private List<Button> Buttons;
    private int currentButtonIndex;
    private FileInfo lastSelectedFile;
    private List<Button> imageButtons;
    private string m_pOpenedPath = null;

    [HideInInspector]
    public FILEOPENFILTER fileOpenFilter = FILEOPENFILTER.IMAGE;
    private enum ButtonType
    {
        File,
        Folder,
        Drive,
        Computer,
    };

    public GameObject FileBrowserWindow; 

    // Files sprites
    public Sprite Folder;
    public Sprite Drive;
    public Sprite File;
    public Sprite Computer;

    // Quick Navigation
    private bool ValidateDoubleClick = false; 
    private Button CurrentlySelected; 

    // Filters
    private List<string[]> FiltersStrings; 

    // Search
    public Text SearchField; 
    public GameObject Loading;
    public GameObject SearchBtn;
    public GameObject AbortSearchBtn;

    // Thumbnails
    private Thread ThumbnailsThread; 
    private FileInfo[] _ThumbnailsFiles = new FileInfo[0]; 
    private List<ThumbData> m_pImagesBytesArray = new List<ThumbData>(); 
    private ThumbData previewThumb; 
    private bool GenerateNewThumbnails = false; 
    private bool m_bGenerationCompleted = false; 
    private bool CancelGeneration = false; 
    private bool CancelThread = false; 
    private int ThumbSize; 
    private int previewThumbSize;
    #endregion

    #region UnityCallBack
    void Awake()
    {
        // Start Thread
        ThreadStart threadDelegate = new ThreadStart(this.GenerateThumbnails);
        ThumbnailsThread = new Thread(threadDelegate);
        ThumbnailsThread.Name = "Thumbnails Thread";
        ThumbnailsThread.Start();

        try
        {
            StartingDirectory = PlayerPrefs.GetString("lastDir");
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
        // Create file browser
        if (StartingDirectory == null || StartingDirectory == string.Empty)
        {
            _FB = new FileBrowser();
        }
        else
        {
            _FB = new FileBrowser(StartingDirectory);
            _FB.lastDirInfo = new DirectoryInfo(StartingDirectory);
        }
        // Initialize the lists
        FiltersStrings = new List<string[]>();
      
        Buttons = new List<Button>();

        // Set the differents file filters
        FiltersStrings.Add(null); // Let this one, it is the default
        FiltersStrings.Add(new string[] { ".jpg", ".jpeg", ".png" }); // You can also specify a list a filters, so any file using any of this extension will be displayed.
    }

    void Start()
    {
        ThumbSize = 180;
        previewThumbSize = 500;
        // Refresh the buttons
        if (StartingDirectory == null) ToRoot();
        ChangeFilters(new string[] { ".jpeg", ".jpg", ".png" });
    }

    void OnApplicationQuit()
    {
            // Cancel Threads
        _FB.QuitSearchThread();
        QuitThumbnailsThread();
    }

    #endregion

    #region UI_MainManagment
    public void Open()
    {
        m_pOpenedPath = null;
        FileBrowserWindow.SetActive(true);

        if (StartingDirectory == null || StartingDirectory == string.Empty)
            _FB.Relocate(null);
        else
            _FB.Relocate(StartingDirectory);

        RefreshButtons();
    }

    public void Open(string Path)
    {
        m_pOpenedPath = null;
        FileBrowserWindow.SetActive(true);

        if (Path == null || Path == string.Empty)
            _FB.Relocate(null);
        else
            _FB.Relocate(Path);

        RefreshButtons();
    }

    public void Cancel()
    {
        FileBrowserWindow.SetActive(false);
    }

    public string GetPath()
    {
        return m_pOpenedPath;
    }

    public void RefreshButtons()
    {
        CancelCurrentThumbnails();
        imageButtons = new List<Button>();
        MainPanel.sizeDelta = Vector2.zero;
      
        // Destroy previous buttons
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].gameObject.SetActive(false);
            DestroyImmediate(Buttons[i].gameObject);
        }

        // Clear List
        Buttons.Clear();

        DirectoryInfo Dir = _FB.GetCurrentDirectory();
        if (Dir == null) pathText.text = "My Computer";
        else pathText.text = Dir.FullName;
        // Retrieve sub directories
        DirectoryInfo[] _childs = _FB.GetChildDirectories();

        // Add a button for each folder
        for (int i = 0; i < _childs.Length; i++)
        {
            // Dissociate folders from drives
            if (_childs[i].Parent != null)
                AddButton(_childs[i].Name, ButtonType.Folder);
            else
                AddButton(_childs[i].Name, ButtonType.Drive);
        }

            // Retrieve files
            FileInfo[] _files = _FB.GetFiles();

            // Add a button for each file
            for (int i = 0; i < _files.Length; i++)
            {
                AddButton(_files[i].Name, ButtonType.File);
            }

            lock (_ThumbnailsFiles)
            {
                _ThumbnailsFiles = _files;
            }

            GenerateNewThumbnails = true;
            StartCoroutine(WaitForThumbnails());
    }

    private void AddButton(string FileName, ButtonType Type)
    {
        // Get height from template button
        float ButtonHeight = MenuItem.gameObject.GetComponent<RectTransform>().rect.height;

        // Create new button
        Button NewButton = GameObject.Instantiate(MenuItem) as Button;
        NewButton.gameObject.SetActive(true);
        NewButton.gameObject.transform.SetParent(MainPanel.gameObject.transform);
        

        // Place button
        RectTransform _CurrentRect = NewButton.gameObject.GetComponent<RectTransform>();
        _CurrentRect.localPosition = new Vector3(0, (-10) * (Buttons.Count + 1) + (-ButtonHeight) * Buttons.Count, 0);
        _CurrentRect.sizeDelta = new Vector2(-20, ButtonHeight);

        // Set button label to retrieve it later
        _CurrentRect.Find("Text").gameObject.GetComponent<Text>().text = FileName;

        // Set button name to store it's type
        switch (Type)
        {
            case ButtonType.File:
                NewButton.gameObject.name = "File";
                _CurrentRect.Find("Image").gameObject.GetComponent<Image>().sprite = File;
                imageButtons.Add(NewButton);
                break;
            case ButtonType.Folder:
                NewButton.gameObject.name = "Folder";
                _CurrentRect.Find("Image").gameObject.GetComponent<Image>().sprite = Folder;
                break;
            case ButtonType.Drive:
                NewButton.gameObject.name = "Drive";
                _CurrentRect.Find("Image").gameObject.GetComponent<Image>().sprite = Drive;
                break;
            case ButtonType.Computer:
                NewButton.gameObject.name = "Computer";
                _CurrentRect.Find("Image").gameObject.GetComponent<Image>().sprite = Computer;
                break;
        }

        // Add the button callback
        int index = imageButtons.Count;
        NewButton.onClick.RemoveAllListeners();
        NewButton.onClick.AddListener(() => SelectFile(NewButton,index));
       

        // Add the button to the list
        Buttons.Add(NewButton);

        // Resize parent panel to fit the new button
        MainPanel.sizeDelta = new Vector2(0, 10 * (Buttons.Count + 1) + ButtonHeight * Buttons.Count);
    }

    #endregion

    #region Shortcuts
        // HIERARCHY

    // Go the the previous file opened
    public void ToPrevious()
    {
        _FB.GoToPrevious();
        RefreshButtons();
    }

    // Go the the next file opened
    public void ToNext()
    {
        _FB.GotToNext();
        RefreshButtons();
    }
    public void Refresh()
    {
        _FB.Refresh();
        RefreshButtons();
    }
    // Go back in the hierarchy
    public void ToParent()
    {
        _FB.GoInParent();
        RefreshButtons();
    }

    // Open root folder
    public void ToRoot()
    {
        _FB.GoToRoot(true);
        RefreshButtons();
    }

        // SPECIAl FOLDERS

    // Open desktop
    public void Desktop()
    {
        string Desktop = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
        _FB.RetrieveFiles(new DirectoryInfo(Desktop), true);
        RefreshButtons();
    }
    #endregion

    #region QuickNavigation
    IEnumerator DoubleClick()
    {
        ValidateDoubleClick = true;

        yield return new WaitForSeconds(.5f);

        ValidateDoubleClick = false;
    }

    //SlideNext
    public void NextImage()
    {
        imageButtons[currentButtonIndex<imageButtons.Count?currentButtonIndex:imageButtons.Count-1].onClick.Invoke();
    }

    //SlidePrivious
    public void PreviousImage()
    {
        imageButtons[currentButtonIndex>=2? currentButtonIndex- 2:0].onClick.Invoke();

    }
    void SelectFile(Button _Button, int imageIndex)
    {
        currentButtonIndex = imageIndex;
        // If file has been double clicked, open it
        if (_Button == CurrentlySelected && ValidateDoubleClick)
        {
            OpenFile();
        }
        // Selecte the file and start the double click cooldown
        else
        {
            CurrentlySelected = _Button;
            if (CurrentlySelected.gameObject.name == "File")
            {
                RectTransform _CurrentRect = CurrentlySelected.gameObject.GetComponent<RectTransform>();
                string _text = _CurrentRect.Find("Text").gameObject.GetComponent<Text>().text;
                lastSelectedFile = new FileInfo(_FB.GetFilePath(_text));
            }
            StartCoroutine(DoubleClick());
        }
    }

 
               
    public void OpenFile()
    {
        if (CurrentlySelected == null)
            return;

        // Retrieve the name of the directory stored in the button label
        RectTransform _CurrentRect = CurrentlySelected.gameObject.GetComponent<RectTransform>();
        string _text = _CurrentRect.Find("Text").gameObject.GetComponent<Text>().text;

        if (CurrentlySelected.gameObject.name == "Folder" || CurrentlySelected.gameObject.name == "Drive")
        {
            // Open the folder if it is one
            _FB.GoInSubDirectory(_text);
            RefreshButtons();
            CurrentlySelected = null;

        }
        else
        {
            if (fileOpenFilter == FILEOPENFILTER.PANORAMA)
            {
                try
                {
                    // Get image
                    Texture2D tex = new Texture2D(2, 2);
                    tex.LoadImage(System.IO.File.ReadAllBytes(lastSelectedFile.FullName));
                    tourController.ChangePaonorama(tex);
                    panelsController.Panels[0].PopBack();
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.Message);
                }
            }
            else if (fileOpenFilter == FILEOPENFILTER.IMAGE)
            {
             
                    // Get image
                    Texture2D tex = new Texture2D(2, 2);
                    tex.LoadImage(System.IO.File.ReadAllBytes(lastSelectedFile.FullName));
                    Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.0f, 0.0f));
                    tourController.AddImageToPoint(sprite);
                    panelsController.Panels[0].PopBack();
          
            }
            else if (fileOpenFilter == FILEOPENFILTER.AUDIO)
            { 
            
            }

        }
    }
    #endregion

    #region FolderManagment
   
    public void SetSortMode(string mode)
    {
            _FB.SetSortMode(FileBrowser.SortingMode.Name);

        RefreshButtons();
     
    }

    #endregion

    #region Filters
    // Callback for the filters button
    public void ChangeFilters(string[] filters)
    {
        _FB.SetFilters(filters);
        RefreshButtons();
    }
    #endregion

    #region Search
    public void Search()
	{
        SearchBtn.SetActive(false);
        AbortSearchBtn.SetActive(true);
        Loading.SetActive(true);
        CancelCurrentThumbnails();
		MainPanel.sizeDelta = Vector2.zero;
		
		// Destroy previous buttons
		for (int i = 0; i < Buttons.Count; i++)
		{
			Buttons[i].gameObject.SetActive( false );
			DestroyImmediate( Buttons[i].gameObject );
		}
		
		// Clear List
		Buttons.Clear ();

		_FB.SearchFor (SearchField.text);

		StartCoroutine (WaitResults ());
	}

    public void StopSearch()
    {
        SearchBtn.SetActive(true);
        AbortSearchBtn.SetActive(false);
        Loading.SetActive(false);
        _FB.m_bSearchCompleted = true;
        RefreshButtons();
    }

	private IEnumerator WaitResults()
	{
		StartCoroutine (_FB.WaitForSearchResult ());

		while( !_FB.IsSearchComplete() )
		{
			RefreshButtons ();
			yield return new WaitForSeconds( .5f );
		}

		RefreshButtons ();
		
		Loading.SetActive (false);
	}
    #endregion

    #region Thumbnails
    // Quit Thread
    public void QuitThumbnailsThread()
    {
        CancelThread = true;

        while (CancelThread)
        {
            Thread.Sleep(20);
        }

        ThumbnailsThread.Abort();
    }

    // Quit Current Generation
    public void CancelCurrentThumbnails()
    {
        if (!GenerateNewThumbnails)
            return;

        CancelGeneration = true;

        while (CancelGeneration)
        {
            Thread.Sleep(20);
        }
    }

    // Main function
    private void GenerateThumbnails()
    {
        while (true)
        {

            if (GenerateNewThumbnails)
            {
                m_bGenerationCompleted = false;

                lock (_ThumbnailsFiles)
                {
                    // Add a button for each file
                    for (int i = 0; i < _ThumbnailsFiles.Length; i++)
                    {
                        if (_ThumbnailsFiles[i].Extension.ToLower() == ".png"
                            || _ThumbnailsFiles[i].Extension.ToLower() == ".jpg"
                            || _ThumbnailsFiles[i].Extension.ToLower() == ".jpeg"
                            || _ThumbnailsFiles[i].Extension.ToLower() == ".gif"
                            || _ThumbnailsFiles[i].Extension.ToLower() == ".bmp")
                        {
                            ThumbData _data = new ThumbData();
                            _data._fullName = _ThumbnailsFiles[i].FullName;
                            _data._name = _ThumbnailsFiles[i].Name;

                            try
                            {
                                System.Drawing.Image image = System.Drawing.Image.FromFile(_data._fullName);
                              

                               

                                if (image.Width == image.Height)
                                {
                                    _data.SizeX = ThumbSize;
                                    _data.SizeY = ThumbSize;
                                }
                                else if (image.Width > image.Height)
                                {
                                    _data.SizeX = ThumbSize;
                                    _data.SizeY = ThumbSize * ((float)image.Height / (float)image.Width);
                                }
                                else if (image.Width < image.Height)
                                {
                                    _data.SizeX = ThumbSize * ((float)image.Width / (float)image.Height);
                                    _data.SizeY = ThumbSize;
                                }

                                System.Drawing.Image thumb = image.GetThumbnailImage((int)_data.SizeX, (int)_data.SizeY, () => false, System.IntPtr.Zero);
                                _data._bytes = imageToByteArray(thumb);

                                thumb.Dispose();
                                image.Dispose();
                            }
                            catch (System.ArgumentException e)
                            {
                                Debug.LogError("ArgumentException when generating thumbnail " + i + ".\n\n" + e.Message);
                            }
                            catch (System.IO.FileNotFoundException e)
                            {
                                Debug.LogError("FileNotFoundException when generating thumbnail " + i + ".\n\n" + e.Message);
                            }
                            catch (System.OutOfMemoryException e)
                            {
                                Debug.LogError("OutOfMemoryException when generating thumbnail " + i + ".\n\n" + e.Message);
                            }
                            catch
                            {
                                Debug.LogError("Unknow error when generating thumbnail " + i + ".\n\n");
                            }

                            lock (m_pImagesBytesArray)
                            {
                                m_pImagesBytesArray.Add(_data);
                            }

                            if (CancelThread || CancelGeneration)
                            {
                                break;
                            }
                        }
                    }
                }

                m_bGenerationCompleted = true;
                GenerateNewThumbnails = false;
            }

            if (CancelThread)
            {
                CancelThread = false;
                break;
            }

            if (CancelGeneration)
            {
                m_bGenerationCompleted = true;
                GenerateNewThumbnails = false;
                lock (m_pImagesBytesArray)
                {
                    m_pImagesBytesArray.Clear();
                }

                lock (_ThumbnailsFiles)
                {
                    _ThumbnailsFiles = new FileInfo[0];
                }
                CancelGeneration = false;
            }

            Thread.Sleep(100);
        }
    }

    // Convert a System.Drawing.Image to a byte array so we can convert it to a Texture2D later
    public byte[] imageToByteArray(System.Drawing.Image imageIn)
    {
        using (var ms = new MemoryStream())
        {
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
    }

    // Coroutine to display new thumbnails at set intervales
    public IEnumerator WaitForThumbnails()
    {
        while (true)
        {
            lock (m_pImagesBytesArray)
            {
                if (m_pImagesBytesArray.Count > 0)
                {
                    for (int i = 0; i < m_pImagesBytesArray.Count; i++)
                    {
                        for (int j = 0; j < Buttons.Count; j++)
                        {
                            RectTransform _CurrentRect = Buttons[j].gameObject.GetComponent<RectTransform>();
                            string _name = _CurrentRect.Find("Text").gameObject.GetComponent<Text>().text;

                            if (m_pImagesBytesArray[i]._name == _name)
                            {
                                Image _pic = Buttons[j].gameObject.transform.Find("Image").gameObject.GetComponent<Image>();

                                Texture2D _thumbnail = new Texture2D(2, 2);

                                _thumbnail.LoadImage(m_pImagesBytesArray[i]._bytes);

                                Sprite _sprite = Sprite.Create(_thumbnail, new Rect(0, 0, _thumbnail.width, _thumbnail.height), new Vector2(0.0f, 0.0f));

                                _pic.sprite = _sprite;
   
                            }

                            if (CancelGeneration || CancelThread)
                            {
                                break;
                            }
                        }
                    }
                }

                m_pImagesBytesArray.Clear();
            }

            if (m_bGenerationCompleted)
            {
                break;
            }
            else
            {
                yield return new WaitForSeconds(0.2f);
            }
        }

        _ThumbnailsFiles = new FileInfo[0];
        m_bGenerationCompleted = false;
    }
    #endregion
}
